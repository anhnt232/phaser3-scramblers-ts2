// ENEMY SPRITE CLASS    

// modules to import
import EnemyGroup from "./enemyGroup";
import PlatformSprite from "./platformSprite";
import { randomValue } from './utils';
import { GameOptions } from './gameOptions';

// enemy sprite extends Arcade Sprite class
export default class EnemySprite extends Phaser.Physics.Arcade.Sprite {

    // the platform where the enemy is patrolling
    platformToPatrol: PlatformSprite;

    // enemy physics body
    body: Phaser.Physics.Arcade.Body;

    // constructor
    // arguments: the game scene, the platform where the enemy is on, and enemy group
    constructor(scene: Phaser.Scene, platform: PlatformSprite, group: EnemyGroup) {
        super(scene, platform.x, platform.y - 100, 'enemy');

        // add the enemy to the scene
        scene.add.existing(this);

        // add physics body to enemy
        scene.physics.add.existing(this);

        // set enemy scale
        this.scale = GameOptions.pixelScale;

        // shrink a bit enemy pyhsics body size to make the game forgive players a bit
        this.body.setSize(this.displayWidth / GameOptions.pixelScale * 0.6, this.displayHeight / GameOptions.pixelScale * 0.75, true);

        // set enemy physics body offset. This has to be done manually, no magic formula
        this.body.setOffset(7, 7)

        // the enemy is patrolling the current platform
        this.platformToPatrol = platform;

        // add the enemy to the group
        group.add(this);

        // set enemy horizontal speed
        this.setVelocityX(randomValue(GameOptions.enemyPatrolSpeedRange) * Phaser.Math.RND.sign());

        // play "enemy_run" animation
        this.anims.play('enemy_run', true);
    }

    // method to make the enemy patrol a platform
    patrol(): void { 

        // flip enemy sprite if moving right
        this.setFlipX(this.body.velocity.x > 0);

        // get platform bounds
        let platformBounds: Phaser.Geom.Rectangle = this.platformToPatrol.getBounds();

        // get enemy bounds
        let enemyBounds: Phaser.Geom.Rectangle = this.getBounds();

        // get enemy horizontal speeds
        let enemyVelocityX: number = this.body.velocity.x
       
        // if the enemy is moving left and is about to fall down the platform to the left side
        // or the enemy is moving right and is about to fall down the platform to the right side
        if ((platformBounds.right + 25 < enemyBounds.right && enemyVelocityX > 0) || (platformBounds.left - 25 > enemyBounds.left && enemyVelocityX < 0)) {

            // invert enemy horizontal speed
            this.setVelocityX(enemyVelocityX * -1);
        }
    }

    // method to remove the enemy from a group and place it into the pool
    // arguments: the group and the pool
    groupToPool(group: EnemyGroup, pool: EnemySprite[]): void {

        // remove enemy from the group
        group.remove(this);

        // push the enemy in the pool
        pool.push(this);  
    }

    // method to remove the enemy from the pool and place it into a group
    // arguments: the platform to patrol and the group
    poolToGroup(platform: PlatformSprite, group: EnemyGroup): void {

        // set the platform to patrol
        this.platformToPatrol = platform;

        // place the enemy in the center of the platform
        this.x = platform.x;

        // place the enemy a little above the platform
        this.y = platform.y - 120;

        // set the enemy visible
        this.setVisible(true);

        // add the enemy to the group
        group.add(this);

        // allow gravity to affect the enemy
        this.body.setAllowGravity(true);

        // set enemy horizontal speed
        this.setVelocityX(randomValue(GameOptions.enemyPatrolSpeedRange) * Phaser.Math.RND.sign());

        // play "enemy_run" animation
        this.anims.play('enemy_run', true);

        // do not vertically flip enemy sprite
        this.setFlipY(false);
    }
}
