// function to toss a random value between two elements in an array
// argument: an array with two items
export function randomValue(a: number[]): number {

    // return a random integer between the first and the second item of the array
    return Phaser.Math.Between(a[0], a[1]);
}