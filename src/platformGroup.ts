// PLATFORM GROUP CLASS  

// modules to import
import PlatformSprite from "./platformSprite";

// platform group extends Arcade Group class
export default class PlatformGroup extends  Phaser.Physics.Arcade.Group {
    
    // constructor
    // arguments: the physics world, the game scene
    constructor(world: Phaser.Physics.Arcade.World, scene: Phaser.Scene) {
        super(world, scene);
    }

    // method to get the lowest platform
    getLowestPlatformY(): number {

        // lowest platform value is initially set to zero
        let lowestPlatformY: number = 0;

        // get all group children
        let platforms: PlatformSprite[] = this.getChildren() as PlatformSprite[];

        // loop through all platforms
        for (let platform of platforms) {

            // get the highest value between lowestPlatform and platform y coordinate
            lowestPlatformY = Math.max(lowestPlatformY, platform.y);
        };

        // return lowest platform coordinate
        return lowestPlatformY;
    }
}