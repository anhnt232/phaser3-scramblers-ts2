// PLATFORM SPRITE CLASS    

// modules to import
import PlatformGroup from './platformGroup';
import { GameOptions } from './gameOptions';
import { randomValue } from './utils';

// platform sprite extends RenderTexture class
export default class PlatformSprite extends Phaser.GameObjects.RenderTexture {

    // platform physics body
    body: Phaser.Physics.Arcade.Body;

    // platform type
    platformType: number = 0;

    // is the platform fading out?
    isFadingOut: Boolean = false;

    // platform group
    platformGroup: PlatformGroup;

    // the three sprites forming the platform: left edge, middle, and right edge
    leftSprite: Phaser.GameObjects.Sprite;
    middleSprite: Phaser.GameObjects.Sprite;
    rightSprite: Phaser.GameObjects.Sprite;

    // constructor
    // arguments: the game scene, the platform group, sprite for left edge, sprite for the middle, sprite for the right edge
    constructor(scene: Phaser.Scene, group: PlatformGroup, leftSprite: Phaser.GameObjects.Sprite, middleSprite: Phaser.GameObjects.Sprite, rightSprite: Phaser.GameObjects.Sprite) {
       super(scene, 0, 0, 1, 16);

        // set left, middle and right sprites
        this.leftSprite = leftSprite;
        this.middleSprite = middleSprite;
        this.rightSprite = rightSprite;

        // RenderTexture object does not have default origin at 0.5, so we need to set it
        this.setOrigin(0.5);

        // add the platform to the scnee
        scene.add.existing(this);

        // add physics body to platform
        scene.physics.add.existing(this);

        // add the platform to group
        group.add(this);

        // platform body does not react to collisions
        this.body.setImmovable(true);

        // platform body is not affected by gravity
        this.body.setAllowGravity(false);

        // save platform group
        this.platformGroup = group;

        // let's initialize the platform, with random position, size and so on
        this.initialize();

        // set platform scale
        this.scale = GameOptions.pixelScale;
    }

    // method to initialize the platform
    initialize(): void {

        // platform is not fading out
        this.isFadingOut = false;

        // platform alpha is set to fully opaque
        this.alpha = 1;

        // get lowest platform Y coordinate
        let lowestPlatformY: number = this.platformGroup.getLowestPlatformY();

        // is lowest platform Y coordinate zero? (this means there are no platforms yet)
        if (lowestPlatformY == 0) {

            // position the first platform
            this.y = GameOptions.gameSize.height * GameOptions.firstPlatformPosition;
            this.x = GameOptions.gameSize.width / 2;
        }
        else {

            // position the platform
            this.y = lowestPlatformY + randomValue(GameOptions.platformVerticalDistanceRange);
            this.x = GameOptions.gameSize.width / 2 + randomValue(GameOptions.platformHorizontalDistanceRange) * Phaser.Math.RND.sign();

            // set a random platform type
            // this.platformType = Phaser.Math.Between(0, 2);
        }

        // platform width
        let newWidth: number = randomValue(GameOptions.platformLengthRange) / GameOptions.pixelScale;
        
        // set platform size
        this.setSize(newWidth, 16);

        // set platform body size
        this.body.setSize(newWidth, 16);
        
        // set middle sprite display width equal to entire platform width
        this.middleSprite.displayWidth = newWidth;

        // draw middle sprite
        this.draw(this.middleSprite, 0, 0);

        // draw left edge sprite
        this.draw(this.leftSprite, 0, 0);

        // draw right edge sprite
        this.draw(this.rightSprite, newWidth, 0);
    }
}
