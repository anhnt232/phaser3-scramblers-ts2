// SAW GROUP CLASS  

// saw group extends Arcade Group class
export default class SawGroup extends  Phaser.Physics.Arcade.Group {
    
    // constructor
    // arguments: the physics world, the game scene
    constructor(world: Phaser.Physics.Arcade.World, scene: Phaser.Scene) {
        super(world, scene);
    }
}