// CLASS TO PRELOAD ASSETS

// this class extends Scene class
export class PreloadAssets extends Phaser.Scene {

    // constructor    
    constructor() {
        super({
            key: 'PreloadAssets'
        });
    }

    // preload assets
    preload(): void {
        this.load.image('platform', 'assets/platform.png');
        this.load.image('background', 'assets/background.png');
        this.load.image('leftplatformedge', 'assets/leftplatformedge.png');
        this.load.image('rightplatformedge', 'assets/rightplatformedge.png');
        this.load.spritesheet('enemy', 'assets/enemy.png', {
            frameWidth: 36,
            frameHeight: 30
        });
        this.load.spritesheet('enemy_hit', 'assets/enemy_hit.png', {
            frameWidth: 36,
            frameHeight: 30
        });
        this.load.spritesheet('hero', 'assets/hero.png', {
            frameWidth: 32,
            frameHeight: 32
        });
        this.load.spritesheet('hero_run', 'assets/hero_run.png', {
            frameWidth: 32,
            frameHeight: 32
        });
        this.load.spritesheet('saw', 'assets/saw.png', {
            frameWidth: 38,
            frameHeight: 38
        });        
	}

    // method to be called once the instance has been created
	create(): void {

        // call PlayGame class
        this.scene.start('PlayGame');
	}
}