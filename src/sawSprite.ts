// SAW SPRITE CLASS    

// modules to import
import PlatformSprite from "./platformSprite";
import { randomValue } from './utils';
import { GameOptions } from './gameOptions';
import SawGroup from "./sawGroup";

// saw sprite extends Arcade Sprite class
export default class SawSprite extends Phaser.Physics.Arcade.Sprite {

    // the platform where the saw is patrolling
    platformToPatrol: PlatformSprite;

    // saw physics body
    body: Phaser.Physics.Arcade.Body;
    
    // constructor
    // arguments: the game scene, the platform where the saw is on, and saw group
    constructor(scene: Phaser.Scene, platform: PlatformSprite, group: SawGroup) {
        super(scene, platform.x, platform.y, 'saw');

        // add the saw to the scene
        scene.add.existing(this);

        // add physics body to enemy
        scene.physics.add.existing(this);

        // set saw scale
        this.scale = GameOptions.pixelScale;

        // play "saw" animation
        this.anims.play('saw', true);

        // add the saw to the group
        group.add(this);

        // determine body size, making it a bit smaller than sprite size
        let bodySize: number = this.displayWidth / 2 / this.scale * 0.8;

        // set saw body circular, and set some offset, manually unfortunately
        this.setCircle(bodySize, 4, 4);

        // saw enemy is patrolling the current platform
        this.platformToPatrol = platform;

        // set saw horizontal speed
        this.setVelocityX(randomValue(GameOptions.sawSpeedRange) * Phaser.Math.RND.sign());

        // set saw vertical speed
        this.setVelocityY(platform.body.velocity.y);

        // saw is not affected by gravity
        this.body.setAllowGravity(false);
    }

    // method to remove the saw from a group and place it into the pool
    // arguments: the group and the pool
    groupToPool(group: SawGroup, pool: SawSprite[]): void {

        // set saw velocity to zero
        this.setVelocity(0 ,0);

        // remove enemy from the group
        group.remove(this);

        // push the enemy in the pool
        pool.push(this);  
    }

    // method to remove the saw from the pool and place it into a group
    // arguments: the platform to patrol and the group
    poolToGroup(platform: PlatformSprite, group: SawGroup): void {

        // set the platform to patrol
        this.platformToPatrol = platform;

        // place the saw in the center of the platform
        this.x = platform.x;
        this.y = platform.y;

        // add the saw to the group
        group.add(this);

        // set saw horizontal speed
        this.setVelocityX(randomValue(GameOptions.sawSpeedRange) * Phaser.Math.RND.sign());

        // set saw vertical speed
        this.setVelocityY(platform.body.velocity.y);
    }

    // method to make the saw patrol a platform
    patrol(): void {

        // get platform bounds
        let platformBounds: Phaser.Geom.Rectangle = this.platformToPatrol.getBounds();

        // get saw bounds
        let sawBounds: Phaser.Geom.Rectangle = this.getBounds();

        // get saw horizontal speed
        let sawVelocityX: number = this.body.velocity.x
       
        // if the saw is moving left and is about to fall down the platform to the left side
        // or the saw is moving right and is about to fall down the platform to the right side
        if ((platformBounds.right + 25 < sawBounds.right && sawVelocityX > 0) || (platformBounds.left - 25 > sawBounds.left && sawVelocityX < 0)) {

            // invert saw horizontal speed
            this.setVelocityX(sawVelocityX * -1);
        }
    }    
}