// CONFIGURABLE GAME OPTIONS

// some constants to store values
export const EMPTY_PLATFORM = 0;
export const ENEMY = 1;
export const SAW = 2;

export const GameOptions = {

    // game size, in pixels
    gameSize: {
        width: 750,
        height: 1334
    },

    // game scale ratio
    pixelScale: 3,

    // first platform vertical position. 0 = top of the screen, 1 = bottom of the screen
    firstPlatformPosition: 4 / 10,

    // game gravity, which only affects the hero
    gameGravity: 1200,

    // hero speed, in pixels per second
    heroSpeed: 300,

    // platform speed, in pixels per second
    platformSpeed: 90,

    // platform length range, in pixels
    platformLengthRange: [150, 250],

    // platform horizontal distance range from the center of the stage, in pixels
    platformHorizontalDistanceRange: [0, 250],

    // platform vertical distance range, in pixels
    platformVerticalDistanceRange: [150, 250],

    // platform tint colors
    platformColors: [0xffffff, 0xff0000, 0x00ff00],

    // bounce velocity when landing on bouncing platform
    bounceVelocity: 500,

    // disappearing platform time before disappearing, in milliseconds
    disappearTime: 1000,

    // enemy patrolling speed range, in pixels per second
    enemyPatrolSpeedRange: [40, 80],

    // saw patrolling speed range, in pixels per second
    sawSpeedRange: [10, 30],

    // array filled with stuff to be added to the platform. An item is randomly picked each time we need to place some stuff on the platform
    platformStuff: [EMPTY_PLATFORM, ENEMY, ENEMY, ENEMY, ENEMY, SAW, SAW, SAW]
}