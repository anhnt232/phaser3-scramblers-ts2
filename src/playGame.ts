// THE GAME ITSELF

// modules to import
import { GameOptions, ENEMY, SAW } from './gameOptions';
import PlayerSprite from './playerSprite';
import PlatformSprite from './platformSprite';
import EnemySprite from './enemySprite';
import PlatformGroup from './platformGroup';
import EnemyGroup from './enemyGroup';
import SawSprite from './sawSprite';
import SawGroup from './sawGroup';

// this class extends Scene class
export class PlayGame extends Phaser.Scene {

    // group to contain all platforms
    platformGroup: PlatformGroup;

    // group to contain all enemies
    enemyGroup: EnemyGroup;

    // group to contain all saws
    sawGroup: SawGroup;

    // the hero of the game
    hero: PlayerSprite;

    // is it the first time player is moving?
    firstMove: Boolean;

    // enemy pool, built as an array
    enemyPool: EnemySprite[];

    // saw pool, built as an array
    sawPool: SawSprite[];

    // just a debug text to print some info
    debugText: Phaser.GameObjects.Text;

    // background image
    backgroundImage: Phaser.GameObjects.TileSprite;

    // left edge platform sprite
    leftPlatform: Phaser.GameObjects.Sprite;

    // right edge platform sprite
    rightPlatform: Phaser.GameObjects.Sprite;

    // middle platform sprite
    middlePlatform: Phaser.GameObjects.Sprite;

    // constructor
    constructor() {
        super({
            key: 'PlayGame'
        });
    }

    // method to be called once the class has been created
    create(): void {

        // method to initialize platform sprites
        this.initializePlatformSprites();

        // method to inizialize animations
        this.initializeAnimations();

        // method to place the background image
        this.setBackground();   

        // add the debug text to the game
        this.debugText = this.add.text(16, 16, '', {
            color: '#000000',
            fontFamily: 'monospace',
            fontSize: '48px'
        });

        // initialize enemy pool as an empty array
        this.enemyPool = [];

        // initialize saw pool as an empty array
        this.sawPool = [];

        // this is the firt move
        this.firstMove = true;

        // create a new physics group for the platforms
        this.platformGroup = new PlatformGroup(this.physics.world, this);

        // create a new physics group for the enemies
        this.enemyGroup = new EnemyGroup(this.physics.world, this);

        // create a new physics group for the saws
        this.sawGroup = new SawGroup(this.physics.world, this);

        // let's create ten platforms. They are more than enough
        for (let i: number = 0; i < 10; i ++) {

            // create a new platform
            let platform: PlatformSprite = new PlatformSprite(this, this.platformGroup, this.leftPlatform, this.middlePlatform, this.rightPlatform);  
            
            // if it's not the first platform...
            if (i > 0) {

                // place some stuff on it
                this.placeStuffOnPlatform(platform);
            }
        }

        // add the hero
        this.hero = new PlayerSprite(this);

        // input listener to move the hero
        this.input.on("pointerdown", this.moveHero, this);

        // input listener to stop the hero
        this.input.on("pointerup", this.stopHero, this);
    }

    // method to set background image
    setBackground(): void {

        // add a tileSprite
        this.backgroundImage = this.add.tileSprite(0, 0, GameOptions.gameSize.width / GameOptions.pixelScale, GameOptions.gameSize.height / GameOptions.pixelScale, 'background');
        
        // set background origin to top left corner
        this.backgroundImage.setOrigin(0, 0);

        // set background scale
        this.backgroundImage.scale = GameOptions.pixelScale;
    }

    // method to inizialize animations
    initializeAnimations(): void {

        // hero idle animation
        this.anims.create({
            key: "idle",
            frames: this.anims.generateFrameNumbers('hero', {
                start: 0,
                end: 10
            }),
            frameRate: 20,
            repeat: -1
        });

        // hero run animation
        this.anims.create({
            key: "run",
            frames: this.anims.generateFrameNumbers('hero_run', {
                start: 0,
                end: 11
            }),
            frameRate: 20,
            repeat: -1
        });

        // enemy run animation
        this.anims.create({
            key: "enemy_run",
            frames: this.anims.generateFrameNumbers('enemy', {
                start: 0,
                end: 11
            }),
            frameRate: 20,
            repeat: -1
        });

        // enemy falling animation
        this.anims.create({
            key: "enemy_falling",
            frames: this.anims.generateFrameNumbers('enemy_hit', {
                start: 0,
                end: 2
            }),
            frameRate: 20
        });

        // saw animation
        this.anims.create({
            key: "saw",
            frames: this.anims.generateFrameNumbers('saw', {
                start: 0,
                end: 7
            }),
            frameRate: 20,
            repeat: -1
        });
    }

    // method to inizialize platform sprites
    initializePlatformSprites(): void {

        // add left platform edge sprite
        this.leftPlatform = this.add.sprite(0, 0, 'leftplatformedge');

        // set registration point to top left corner
        this.leftPlatform.setOrigin(0, 0);

        // set sprite to invisible
        this.leftPlatform.setVisible(false);

        // add right platform edge sprite
        this.rightPlatform = this.add.sprite(0, 0, 'rightplatformedge');

        // set registration point to top right corner
        this.rightPlatform.setOrigin(1, 0);

        // set sprite to invisible
        this.rightPlatform.setVisible(false);

        // add middle platform sprite
        this.middlePlatform = this.add.sprite(0, 0, 'platform');

        // set registration point to center
        this.middlePlatform.setOrigin(0, 0);

        // set sprite to invisible
        this.middlePlatform.setVisible(false);
    }

    // method to place stuff on platform
    // argument: the platform
    placeStuffOnPlatform(platform: PlatformSprite): void {

        // pick a random item from platformStuff array
        let randomPick: number = Phaser.Utils.Array.GetRandom(GameOptions.platformStuff);

        // add stuff according to random pick
        switch (randomPick) {

            // enemy
            case ENEMY: { 
                
                // is the enemy pool empty?
                if (this.enemyPool.length == 0) {

                    // create a new enemy sprite
                    new EnemySprite(this, platform, this.enemyGroup);
                }

                // enemy pool is not empty
                else {

                    // retrieve an enemy from the enemy pool
                    let enemy: EnemySprite = this.enemyPool.shift() as EnemySprite;

                    // move the enemy from the pool to enemy group
                    enemy.poolToGroup(platform, this.enemyGroup);
                }
                break;
            }

            // saw
            case SAW: {

                // is the saw pool empty?
                if (this.sawPool.length == 0) {
                    
                    // create a new saw sprite
                    new SawSprite(this, platform, this.sawGroup);
                }

                // saw pool is not empty
                else {

                    // retrieve a saw from the saw pool
                    let saw: SawSprite = this.sawPool.shift() as SawSprite;

                    // move the saw from the pool to saw group
                    saw.poolToGroup(platform, this.sawGroup);

                }
                break;
            }
        }
    }

    // method to move the hero
    // argument: the input pointer
    moveHero(e: Phaser.Input.Pointer): void {

        // set hero movement according to input position
        this.hero.setMovement((e.x > GameOptions.gameSize.width / 2) ? this.hero.RIGHT : this.hero.LEFT);

        // is it the first move?
        if (this.firstMove) {

            // it's no longer the first move
            this.firstMove = false;

            // move platform group
            this.platformGroup.setVelocityY(-GameOptions.platformSpeed);

            // move saw group
            this.sawGroup.setVelocityY(-GameOptions.platformSpeed);
        }
    }

    // method to stop the hero
    stopHero(): void {

        // ... just stop the hero :)
        this.hero.setMovement(this.hero.STOP);
    }

    // method to handle collisions between hero and saws
    // arguments: the two colliding bodies
    handleSawCollision(body1: Phaser.GameObjects.GameObject, body2: Phaser.GameObjects.GameObject): void {
        
        // restart the game
        this.scene.start("PlayGame");           
    }

    // method to handle collisions between hero and enemies
    // arguments: the two colliding bodies
    handleEnemyCollision(body1: Phaser.GameObjects.GameObject, body2: Phaser.GameObjects.GameObject): void {

        // first body is the hero
        let hero: PlayerSprite = body1 as PlayerSprite;
 
        // second body is the enemy
        let enemy: EnemySprite = body2 as EnemySprite;

        // the following code will be executed only if the hero touches the enemy on its upper side (STOMP!)
        if (hero.body.touching.down && enemy.body.touching.up) {

            // move the enemy from enemy group to enemy pool
            enemy.groupToPool(this.enemyGroup, this.enemyPool);

            // play "enemy_falling" animation
            enemy.anims.play('enemy_falling', true);

            // flip the enemy vertically
            enemy.setFlipY(true);

            // make the hero bounce
            hero.setVelocityY(GameOptions.bounceVelocity * -1);
        }

        // hero touched an enemy without stomping it
        else {

            // restart the game
            this.scene.start("PlayGame");
        }
    }

    // method to handle collisions between hero and platforms
    // arguments: the two colliding bodies
    handlePlatformCollision(body1: Phaser.GameObjects.GameObject, body2: Phaser.GameObjects.GameObject): void {
 
        // first body is the hero
        let hero: PlayerSprite = body1 as PlayerSprite;
 
        // second body is the platform
        let platform: PlatformSprite = body2 as PlatformSprite;

        // the following code will be executed only if the hero touches the platform on its upper side
        if (hero.body.touching.down && platform.body.touching.up) {

            // different actions according to platform type
            switch (platform.platformType) {

                // breakable platform
                case 1:

                    // if the platform is not already fading out...
                    if (!platform.isFadingOut) {

                        // flag the platform as a fading out platform
                        platform.isFadingOut = true;

                        // add a tween to fade the platform out
                        this.tweens.add({
                            targets: platform,
                            alpha: 0,
                            ease: 'bounce',
                            duration: GameOptions.disappearTime,
                            callbackScope: this,
                            onComplete: function() {

                                // reset the platform
                                this.resetPlatform(platform);
                            }
                        });
                    }
                    break;
                
                // bouncy platform
                case 2:

                    // make the hero jump changing vertical velocity
                    hero.setVelocityY(GameOptions.bounceVelocity * -1);
                    break;
            }
        }
    }

    // method to reset a platform
    // argument: the platform
    resetPlatform(platform: PlatformSprite): void {
          
        // recycle the platform
           platform.initialize();

        // place stuff on platform
        this.placeStuffOnPlatform(platform);
    }

    // method to handle collisions between enemies and platforms
    // arguments: the two colliding bodies
    handleEnemyPlatformCollision(body1: Phaser.GameObjects.GameObject, body2: Phaser.GameObjects.GameObject): void {
 
        // first body is the enemy
        let enemy: EnemySprite = body1 as EnemySprite;
 
        // second body is the platform
        let platform: PlatformSprite = body2 as PlatformSprite;

        // set the platform to patrol
        enemy.platformToPatrol = platform;
    }

    // method to be executed at each frame
    update(): void {

        // if the hero is already moving...
        if (!this.firstMove) {

            // scroll a bit the background texture
            this.backgroundImage.tilePositionY += 0.2;
        }

        // move the hero
        this.hero.move();       

        // handle collision between hero and platforms
        this.physics.world.collide(this.hero, this.platformGroup, this.handlePlatformCollision, undefined, this);

        // handle collision between enemies and platforms
        this.physics.world.collide(this.enemyGroup, this.platformGroup, this.handleEnemyPlatformCollision, undefined, this);

        // handle collisions between hero and enemies
        this.physics.world.collide(this.hero, this.enemyGroup, this.handleEnemyCollision, undefined, this);

        // handle collisions between hero and saws
        this.physics.world.collide(this.hero, this.sawGroup, this.handleSawCollision, undefined, this);

        // get all platforms
        let platforms: PlatformSprite[] = this.platformGroup.getChildren() as PlatformSprite[];

        // loop through all platforms
        for (let platform of platforms) {

            // get platform bounds
            let platformBounds: Phaser.Geom.Rectangle = platform.getBounds();

            // if a platform leaves the stage to the upper side...
            if (platformBounds.bottom < 0) {

                // reset the platform
                this.resetPlatform(platform);
            }
        }

        // get all saws
        let saws: SawSprite[] = this.sawGroup.getChildren() as SawSprite[];

        // loop through all saws
        for (let saw of saws) {   

            // make enemy patrol
            saw.patrol();

            let sawBounds: Phaser.Geom.Rectangle = saw.getBounds();

            // if a saw leaves the stage to the upper side...
            if (sawBounds.bottom < 0) {

                // move the saw from saw group to saw pool
                saw.groupToPool(this.sawGroup, this.sawPool);
            }
        }

        // get all enemies
        let enemies: EnemySprite[] = this.enemyGroup.getChildren() as EnemySprite[];

        // update debug text
        this.debugText.setText("Enemies: group " + enemies.length.toString() + ", pool: " + this.enemyPool.length.toString() + "\nSaws: group " + saws.length.toString() + ", pool: " + this.sawPool.length.toString());

        // loop through all enemies in enemyPool array
        for (let enemy of this.enemyPool) {

            // if the enemy falls down the stage...
            if (enemy.y > GameOptions.gameSize.height + 100) {

                // set enemy velocity to zero
                enemy.setVelocity(0 ,0);

                // do not let enemy to be affacted by gravity
                enemy.body.setAllowGravity(false);
            }
        }

        // loop through all enemies
        for (let enemy of enemies) {   

            // make enemy patrol
            enemy.patrol();

            // get enemy bounds
            let enemyBounds: Phaser.Geom.Rectangle = enemy.getBounds();

            // if the enemy leaves the screen...
            if (enemyBounds.bottom < 0) {

                // move enemy from enemy group to enemy pool
                enemy.groupToPool(this.enemyGroup, this.enemyPool);

                // do not show the enemy
                enemy.setVisible(false); 
            }
        }
        
        // if the hero falls down or leaves the stage from the top...
        if (this.hero.y > GameOptions.gameSize.height || this.hero.y < 0) {

            // restart the scene
            this.scene.start("PlayGame");
        }
    }
}