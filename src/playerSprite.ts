// PLAYER SPRITE CLASS  

// modules to import
import { GameOptions } from './gameOptions';

// player sprite extends Arcade Sprite class
export default class PlayerSprite extends Phaser.Physics.Arcade.Sprite {

    // assign LEFT property a -1 value, just like if it were a constant
    LEFT: number = -1;

    // assign RIGHT property a 1 value, just like if it were a constant
    RIGHT: number = 1;

    // assign STOP property a 0 value, just like if it were a constant
    STOP: number = 0;

    // player current movment is STOP
    currentMovement: number = this.STOP;

    // constructor
    // argument: game scene
	constructor(scene: Phaser.Scene) {
		super(scene, GameOptions.gameSize.width / 2, GameOptions.gameSize.height * GameOptions.firstPlatformPosition - 100, 'hero');

        // add the player to the scnee
        scene.add.existing(this);

        // add physics body to platform
        scene.physics.add.existing(this);

        // set player scale
        this.scale = GameOptions.pixelScale;

        // shrink a bit player pyhsics body size to make the game forgive players a bit
        this.body.setSize(this.displayWidth / GameOptions.pixelScale * 0.6, this.displayHeight / GameOptions.pixelScale * 0.7, false);

        // set player physics body offset. This has to be done manually, no magic formula
        this.body.setOffset(7, 9);
	}

    // method to set player movement
    // argument: the new movement
    setMovement(n: number): void {

        // set currentMovement to n
        this.currentMovement = n;
    }

    // method to move the player
    move(): void {

        // set player horizontal velocity according to currentMovement value
        this.setVelocityX(GameOptions.heroSpeed * this.currentMovement);
        
        // various cases according to player movememt
        switch (this.currentMovement) {

            // player is moving left
            case this.LEFT:

                // flip sprite horizontally
                this.setFlipX(true);

                // play "run" animation
                this.anims.play('run', true);
                break;

            // player is moving right
            case this.RIGHT:

                // do not flip sprite horizontally
                this.setFlipX(false);

                // play "run" animation
                this.anims.play('run', true);
                break;

            // player is not moving
            case this.STOP:

                // play "idle" animation
                this.anims.play('idle', true);
                break;
        }
    }
}